﻿

CREATE TABLE page
(
    id serial primary key,
    page_uri character varying unique
);


CREATE TABLE page_word
(
    id serial primary key,
    word character varying,
    page_id integer references page(id)
);


CREATE TABLE page_link
(
    id serial primary key,
    link character varying,
    page_id integer references page(id)
);


CREATE TABLE page_tf_weight
(
    id serial primary key,
    tf_weight double precision,
    page_id integer references page(id)
);


CREATE TABLE page_idf_weight
(
    id serial primary key,
    idf_weight double precision,
    page_id integer references page(id)
);


CREATE TABLE vector
(
    id serial primary key,
    word character varying,
    weight double precision,
    page_id integer references page(id)
);