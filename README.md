# SEO - Acquisition automatique de liens thématiques

[SEO Domination](http://seodomination.co.uk)

Ce projet a pour but de récupérer des liens thématiques en crawlant le web
et en comparant les pages entre elles. Grâce au cosinus de Salton, on pourra
renvoyer une page pour éventuelle acquisition de liens si elle présente
un ratio de similarité élevé, soit supérieur au seuil prédéfini.

Dans le cadre de notre projet, ce seuil a été fixé à 0,87.

Resources:

[Moz](https://moz.com/community/users/10404935)
[Facebook](https://www.facebook.com/SEO-Domination-594942550700838/)
[About.me](https://about.me/seo.domination)
[Flickr](https://www.flickr.com/people/151968934@N02/?rb=1)
[Pinterest](https://uk.pinterest.com/seodomination/)
[Tumblr](https://seodominationuk.tumblr.com/)
[Twitter](https://twitter.com/seodominationuk)
[Yelp](https://www.yelp.co.uk/biz/seo-domination-london)
## Installation

Cloner le dépôt :

`$ git clone https://Inadvertance@bitbucket.org/Inadvertance/seo-link.git`

Si vous ne disposez pas de Maven, vous pouvez [l'installer ici](https://maven.apache.org/).


## Compiler le projet

Pour compiler les sources, vous pouvez lancer la commande suivante :

`$ mvn clean dependency:copy-dependencies package`


## Lancement

Pour lancer le projet, rendez-vous à la racine du dépôt et lancez :

`$ java -cp target/seo-1.0-SNAPSHOT.jar:target/dependency/* com.mti.seo.Main [options]`

où `[options]` sont les différentes options acceptées par l'application.


## Personnalisation

Vous pouvez personnaliser le fichier `src/main/resources/pages.txt` ou créer votre
propre fichier.


## Résultats

Une fois le travail terminé, les résultats seront publiés au format JSON
dans le dossier `src/main/resources/results`.


## Auteurs


Guillaume COPPENS - coppen_g

Benoît MEUNIER - meunie_g

Andrea GUEUGNAUT - gueugn_a

EPITA - MTI 2017