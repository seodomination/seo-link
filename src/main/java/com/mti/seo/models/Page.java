package com.mti.seo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Page {

    private String pageUri;

    private List<String> words;
    private List<String> links;
    private Map<String, Float> vectors;
    private List<Float> tfWeights;
    private List<Float> idfWeights;

    public Page(String uri, List<String> words, List<String> urisList/*, Document doc*/) {
        this.pageUri = uri;
        this.words = words;
        this.links = urisList;
    }
}
