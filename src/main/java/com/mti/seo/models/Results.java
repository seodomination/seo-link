package com.mti.seo.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Created by coppen_g
 */
@Data
@AllArgsConstructor
public class Results {

    private SimplePage reference;
    private List<SimplePage> savedPages;
}
