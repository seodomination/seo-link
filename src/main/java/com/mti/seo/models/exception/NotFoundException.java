package com.mti.seo.models.exception;

/**
 * Created by coppen_g
 */
public class NotFoundException extends Exception {

    public NotFoundException(String message) {
        super(message);
    }

}
