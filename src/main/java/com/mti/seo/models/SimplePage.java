package com.mti.seo.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

/**
 * Created by coppen_g
 */
@Data
@AllArgsConstructor
public class SimplePage {

    public String url;
    public Map<String, Float> weights;

    public static SimplePage convertFromPage(Page page) {
        return new SimplePage(page.getPageUri(), page.getVectors());
    }

}
