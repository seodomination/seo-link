package com.mti.seo;

import com.mti.seo.interpreter.BaseInterpreter;
import com.mti.seo.interpreter.Interpreter;

public class Main {
  public static void main(String[] args) {
    BaseInterpreter interpreter = new Interpreter();
    interpreter.configure();
    interpreter.interpret(args);
  }
}
