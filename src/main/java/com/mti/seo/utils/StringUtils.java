package com.mti.seo.utils;

import java.text.Normalizer;

/**
 * Created by coppen_g
 */
public class StringUtils {

    public static String removeAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("\\p{M}", "");
        return s;
    }

}
