package com.mti.seo.crawler;

import java.util.Queue;

public abstract class AbstractCrawler implements BaseCrawler {

  protected Queue<String> urls;

  public AbstractCrawler(Queue<String> urls) {
    this.urls = urls;
  }
}
