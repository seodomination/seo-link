package com.mti.seo.crawler;

import com.mti.seo.models.Page;
import com.mti.seo.models.exception.NotFoundException;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.List;
import java.util.Queue;

public interface BaseCrawler {

  /**
   * Indexes page words and links.
   *
   * @param uri The URI of the page.
   * @param words The relevant words in the page.
   * @param urisList The URIs contained in the page.
   * @return the page added.
   */
  public Page addToIndex(String uri, List<String> words, List<String> urisList/*, Document doc*/);

  /**
   * Indexes a page.
   *
   * @param page The page to add.
   * @return the page added.
   */
  public Page addPageToIndex(Page page);

  /**
   * Enqueues the given list of URIs.
   *
   * @param links The URIs to add.
   */
  public boolean addToURIs(List<String> links);

  /**
   * Launches the whole crawl chain.
   */
  public void crawl();

  /**
   * Extracts the URIs contained in the page.
   *
   * @param doc The page from which we want to extract the URIs.
   * @return The list of URIs contained in the page.
   */
  public List<String> extractURIs(Document doc);


  /**
   * Extracts the words contained in the page.
   *
   * @param doc The page from which we want to extract the words.
   * @return The list of words of the page.
   */
  public List<String> extractWords(Document doc);


  /**
   * Gets the page which matches the given URI.
   *
   * @param uri The URI to download from.
   * @return The newly build page.
   */
  public Page getPage(String uri) throws IOException;

  /**
   * Dequeues the first URI.
   *
   * @param uris The queue of URIs.
   * @return The first URI in the queue, if any.
   * @throws
   */
  public String getURI(Queue<String> uris) throws NotFoundException;
}
