package com.mti.seo.crawler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mti.seo.config.CrawlerConfig;
import com.mti.seo.models.Page;
import com.mti.seo.models.Results;
import com.mti.seo.models.SimplePage;
import com.mti.seo.vectorizer.Comparator;
import com.mti.seo.vectorizer.Lemmatizer;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Crawler extends WebCrawler {
    private Lemmatizer lemmatizer = new Lemmatizer();
    private Comparator comparator = new Comparator();
    private final String resultFilePath = String.format("src/main/resources/results/crawl_%s.json", LocalDateTime.now());
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public boolean shouldVisit(edu.uci.ics.crawler4j.crawler.Page page, WebURL url) {
        //System.err.println(String.format("[DEBUG][Crawler:27] Found the following url to visit : %s", url.getURL().toLowerCase()));
        return super.shouldVisit(page, url);
    }

    @Override
    public void visit(edu.uci.ics.crawler4j.crawler.Page page) {
        final String pageUrl = page.getWebURL().getURL();
        System.err.println(String.format("[DEBUG][Crawler:34] Currently visiting %s", pageUrl));

        if (page.getParseData() instanceof HtmlParseData) {
            Page pageToCompute;
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String pageText = htmlParseData.getText();

            // Get the external links of the page
            List<String> outGoingLinks = htmlParseData.getOutgoingUrls().stream().map(WebURL::getURL).collect(Collectors.toList());
            // Parse and extract the words from the page
            List<String> pageWords = this.extractWords(pageText);
            pageToCompute = new Page(pageUrl, pageWords, outGoingLinks);


            if (pageToCompute.getPageUri().equals(CrawlerConfig.getReferenceUrl())) {

                CrawlerConfig.setReferencePage(pageToCompute);
                lemmatizer.execute(pageToCompute, true);
            } else {
                // As the current page is not the reference, we need to update said-reference in
                lemmatizer.execute(pageToCompute, false);

                synchronized (CrawlerConfig.getReferencePage()) {
                    if (comparator.compare(CrawlerConfig.getReferencePage(), pageToCompute) != null) {
                        CrawlerConfig.getSavedPages().add(pageToCompute);
                        // If we add a new Page to our list, we need to compute the new idf weights for
                        // the reference page and to recreate its vector
                        lemmatizer.computeIDFWeights(CrawlerConfig.getReferencePage(), CrawlerConfig.getSavedPages(), true);
                        lemmatizer.createPageVector(CrawlerConfig.getReferencePage());
                    }
                }
            }
        }
    }

    /**
     * We use the onBefore exit method of the {@link WebCrawler} class to write down the reference page
     * and the saved ones in the resource folder of the project
     * <p>
     * The written objects are not complete, only the interesting parts (uri and weight vector) are left
     */
    @Override
    public void onBeforeExit() {
        Results finalResults = new Results(
                SimplePage.convertFromPage(CrawlerConfig.getReferencePage()),
                CrawlerConfig.getSavedPages().stream().map(SimplePage::convertFromPage).collect(Collectors.toList())
        );


        try {
            File resultFile = new File(resultFilePath.replace(":", "-"));
            File parent = resultFile.getParentFile();

            if (!parent.exists() && !parent.mkdir())
                throw new IOException(String.format("Impossible de create directories for path : %s", this.resultFilePath));

            this.objectMapper.writeValue(resultFile, finalResults);
        } catch (IOException e) {
            System.err.println(String.format("[ERROR][CRAWLER:76] Unable to write file to path : %s", this.resultFilePath));
            e.printStackTrace();
        }

        super.onBeforeExit();
    }

    public List<String> extractWords(String doc) {
        String[] array = doc.split("[\\s,.'\"{}()]+");

        List<String> res = new ArrayList<String>();
        for (String s : array)
            res.add(s);

        return res;
    }

}
