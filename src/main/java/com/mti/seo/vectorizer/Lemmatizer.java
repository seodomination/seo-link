package com.mti.seo.vectorizer;

import com.mti.seo.config.CrawlerConfig;
import com.mti.seo.models.Page;
import com.mti.seo.models.exception.NotFoundException;
import com.mti.seo.utils.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lemmatizer implements BaseLemmatizer {

  public Lemmatizer() {
    super();
  }

  /**
   * Method used by the computeIDFWeights. It will simply count the number of times a given
   * word appears in a list of pages and the reference page.
   *
   * @param word the word to search for
   * @param pageUri the identifier of the page, to be sure we don't count the word twice
   * @param pages the list of {@link Page} in which to search for the given word
   * @return The number of times the word was found inside the given list of {@link Page}
   */
  private Float countNbOfOccurrences(String word, String pageUri, List<Page> pages) {

    Float nbOccurrences = 1F;
    List<String> pageWords = new ArrayList<>();

    for (Page page : pages) {

      if (!page.getPageUri().equals(pageUri)) {
        pageWords = page.getWords();

        if (pageWords.contains(word))
          nbOccurrences++;
      }
    }

    Page referencePage = CrawlerConfig.getReferencePage();

    if (referencePage.getPageUri().equals(pageUri) || !referencePage.getWords().contains(word))
        return nbOccurrences;
    else
        return (nbOccurrences + 1);
  }

  // Apply weights : compute TF value
  public List<Float> applyWeights(List<String> words) {
    List<String> markedWords = new ArrayList<>();
    List<Float> tfWeights = new ArrayList<>();
    final Integer wordCount = words.size();

    for (String word : words) {

      if (!markedWords.contains(word)) {
        markedWords.add(word);
        tfWeights.add(1F / wordCount);
      }
      else {
        final Integer wordIndex = markedWords.indexOf(word);
        tfWeights.set(wordIndex, tfWeights.get(wordIndex) + (1F / wordCount));
      }
    }

    return tfWeights;
  }

  public void execute(Page page, Boolean isReferencePage) {
    List<String> words = lemmatize(page.getWords());
    List<Float> tfWeights = this.applyWeights(words);

    page.setWords(words);
    page.setTfWeights(tfWeights);
    this.computeIDFWeights(page, CrawlerConfig.getSavedPages(), isReferencePage);
    this.createPageVector(page);
  }

  public List<String> lemmatize(List<String> words) {
    Stream<String> stream = words.stream().map(word -> findMatch(word));
    return stream.collect(Collectors.toList());
  }

  public String findMatch(String word) {
    String path = String.format("dico/dico_%s.txt", StringUtils.removeAccents(word.substring(0, 1)));
    InputStream in = getClass().getClassLoader().getResourceAsStream(path);

    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(in, "windows-1252"));
      String line = reader.readLine();
      String[] array;

      while (line != null && !line.isEmpty()) {
        // Lemmatize current word
        array = line.split("\\s+");
        if (array[0].equalsIgnoreCase(word)) {
          return array[1];
        }
        line = reader.readLine();
      }
      throw new NotFoundException("String " + word + " not found while " +
      "lemmatizing... Skipping.");
    } catch (IOException e) {
      //System.err.println(e.getMessage());
      return "";
    } catch (NotFoundException | NullPointerException e) {
      //System.err.println(e.getMessage());
      return word;
    }
  }

  /**
   * The main method used to compute the IDF for each of a given page words, according
   * to the words present in the list of saved pages
   *
   * @param pageToCompute The {@link Page} on which we want to compute the IDF
   * @param pages The list of saved {@link Page}
   * @param isReferencePage A boolean to properly compute the total number of documents
   */
  public void computeIDFWeights(Page pageToCompute, List<Page> pages, Boolean isReferencePage) {
      List<String> words = pageToCompute.getWords();
      Integer documentCounts = 1;
      Map<String, Float> weights = new HashMap<>();

    // The +1 to the savedPage size is to count the reference and the current page amongst the savedPages
      if (!isReferencePage)
        documentCounts = CrawlerConfig.getSavedPages().size() + 2;

      for (String word : words) {
          weights.putIfAbsent(word, this.countNbOfOccurrences(word, pageToCompute.getPageUri(), pages) / documentCounts);
      }

      pageToCompute.setIdfWeights(new ArrayList<>(weights.values()));
     }

  /**
   * The method below is used to create the weights vector for a given page
   *
   * @param page {@link Page}
   */
  public void createPageVector(Page page) {
    List<String> words = page.getWords();
    List<Float> tfWeights = page.getTfWeights();
    List<Float> idfWeights = page.getIdfWeights();
    Map<String, Float> vector = new Hashtable<>();

    Integer lastWeightsIndex = 0;

    for (String word : words) {

      if (vector.get(word) == null) {
        Float finalWeight = tfWeights.get(lastWeightsIndex) * idfWeights.get(lastWeightsIndex);
        vector.put(word, finalWeight);
        lastWeightsIndex++;
      }
    }

    page.setVectors(vector);
  }
}
