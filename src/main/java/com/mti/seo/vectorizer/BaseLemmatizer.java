package com.mti.seo.vectorizer;

import com.mti.seo.models.Page;

import java.util.Dictionary;
import java.util.List;

public interface BaseLemmatizer {
  /**
   * Assigns a weight to each word.
   * @param words Test
   * @return A dictionary where keys correspond to words and values
   * correspond to their weight.
   */
  public List<Float> applyWeights(List<String> words);

  public void execute(Page page, Boolean isReferencePage);

  public List<String> lemmatize(List<String> words);
}
