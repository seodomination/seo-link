package com.mti.seo.vectorizer;


import com.mti.seo.models.Page;

/**
 * Created by coppen_g
 */
public interface BaseComparator {

    public Page compare(Page page1, Page page2);

}
