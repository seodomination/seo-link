package com.mti.seo.vectorizer;

import com.mti.seo.models.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coppen_g
 */
public class Comparator implements BaseComparator {

    private Float computeVectorLength(List<Float> vector) {

        Double vectorLength = 0D;

        for (Float elem : vector) {
            vectorLength += (elem *  elem);
        }
        vectorLength = Math.sqrt(vectorLength);

        return vectorLength.floatValue();
    }

    private Float computeScalarProduct(List<Float> vector1, List<Float> vector2) {
        Float scalarProduct = 0F;

        for (Integer i = 0; i < vector1.size() && i < vector2.size(); i++) {
            scalarProduct += vector1.get(i) * vector2.get(i);
        }

        return scalarProduct;
    }

    private Float computeSaltonCosine(List<Float> vector1, List<Float> vector2) {

        Float vector1Length = this.computeVectorLength(vector1);
        Float vector2Length = this.computeVectorLength(vector2);
        Float scalarProduct = Math.abs(this.computeScalarProduct(vector1, vector2));

        return scalarProduct / (vector1Length * vector2Length);
    }

    @Override
    public Page compare(Page page1, Page page2) {
        final Float threshold = 0.8F;
        final Float cosine = this.computeSaltonCosine(
                new ArrayList<>(page1.getVectors().values()),
                new ArrayList<>(page2.getVectors().values())
        );

        if (cosine > threshold)
            return page1;
        else
            return null;
    }
}
