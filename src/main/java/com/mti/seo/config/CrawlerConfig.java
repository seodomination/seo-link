package com.mti.seo.config;

import com.mti.seo.models.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coppen_g
 */
public class CrawlerConfig {

    private static String referenceUrl;
    private static Page referencePage;
    private static List<Page> savedPages = new ArrayList<>();
    private static Integer crawledDocumentsNumber;

    public static void setReferenceUrl(String referenceUrl) {
        CrawlerConfig.referenceUrl = referenceUrl;
    }

    public static String getReferenceUrl() {
        return CrawlerConfig.referenceUrl;
    }

    public static void setReferencePage(Page referencePage) {
        CrawlerConfig.referencePage = referencePage;
    }

    public static Page getReferencePage() {
        return CrawlerConfig.referencePage;
    }

    public static List<Page> getSavedPages() {
        return CrawlerConfig.savedPages;
    }

    public static Integer getCrawledDocumentsNumber() {
        return CrawlerConfig.crawledDocumentsNumber;
    }

    public static void setCrawledDocumentsNumber(Integer crawledDocumentsNumber) {
        CrawlerConfig.crawledDocumentsNumber = crawledDocumentsNumber;
    }
}
