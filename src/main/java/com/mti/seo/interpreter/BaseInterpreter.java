package com.mti.seo.interpreter;

import com.mti.seo.interpreter.commands.Command;

import java.util.List;

public interface BaseInterpreter {
  public void configure();

  public void interpret(String[] args);

  public void executeCommands(List<Command> commands);
}
