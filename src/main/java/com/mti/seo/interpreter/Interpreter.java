package com.mti.seo.interpreter;

import com.mti.seo.interpreter.commands.*;
import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Interpreter implements BaseInterpreter {
    private Options options = null;

    public void configure() {
        this.options = new Options();
        Option helpOption = new Option("h", "help", false, "Display the list of commands and their descriptions");
        Option versionOption = new Option("v", "version", false, "Display the com.mti.seo.crawler's version");
        Option crawlWithFileOption = new Option("f", "file", true, "Crawls using a file containing the list of urls to crawl");
        Option crawlWithoutFileOption = new Option("u", "url", true, "Crawls using the url given as parameter");

        this.options.addOption(helpOption);
        this.options.addOption(versionOption);
        this.options.addOption(crawlWithFileOption);
        this.options.addOption(crawlWithoutFileOption);
    }

    public void interpret(String[] args) {
        if (this.options == null)
            this.configure();

        List<Command> commands = new ArrayList<Command>();
        CommandLineParser commandLineParser = new DefaultParser();

        try {
            CommandLine commandLine = commandLineParser.parse(this.options, args);
            List<Option> clOptions = Arrays.asList(commandLine.getOptions());

            if (clOptions.stream().filter((Option option) -> option.getOpt().equals("h")).findAny().isPresent()) {
                commands.add(new HelpCommand(this.options));
            } else {
                if (clOptions.stream().filter((Option option) -> option.getOpt().equals("v")).findAny().isPresent()) {
                    commands.add(new VersionCommand());
                } else {
                    for (Option option : clOptions) {
                        if (option.getOpt().equals("f"))
                            commands.add(new CrawlWithFileCommand(commandLine.getOptionValue("f")));
                        else
                            commands.add(new CrawlWithoutFileCommand(commandLine.getOptionValue("u")));
                    }
                }
            }

            if (!commands.isEmpty())
                this.executeCommands(commands);
            else {
                System.err.println("Options not recognized");
                System.exit(127);
            }

        } catch (ParseException e) {
            System.err.println(String.format("Unable to parse command, reason : %s", e.getMessage()));
            System.exit(127);
        }
    }

    public void executeCommands(List<Command> commands) {
        commands.forEach(cmd -> cmd.execute());
    }
}
