package com.mti.seo.interpreter.commands;

import com.mti.seo.config.CrawlerConfig;
import com.mti.seo.crawler.Crawler;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

import java.io.*;
import java.util.LinkedList;
import java.util.Queue;

public class CrawlWithFileCommand implements Command {

    private String path;

    public CrawlWithFileCommand(String path) {
        this.path = path;
    }

    /**
     * @param path The path to get the file containing the urls to crawl
     * @return a queue containing the urls to crawl
     */
    private Queue<String> getUrlsFromFile(String path) {
        File file = new File(path);
        Queue<String> urls = new LinkedList<>();
        String readUrl;

        if (file.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                readUrl = br.readLine();

                if (readUrl != null && !readUrl.isEmpty()) {
                    CrawlerConfig.setReferenceUrl(readUrl);
                    urls.add(readUrl);

                    while ((readUrl = br.readLine()) != null && !readUrl.isEmpty()) {
                        urls.add(readUrl);
                    }
                }

                return urls;

            } catch (FileNotFoundException e) {
                System.err.println("The given file does not exist");
            } catch (IOException e) {
                System.err.println(String.format("An error occurred during file parse : %s", e.getMessage()));
            }

        } else {
            System.err.println("The given file does not exist");
        }

        return null;
    }

    public void execute() {
        Queue<String> urls = this.getUrlsFromFile(this.path);

        if (urls == null || urls.size() == 0)
            return;

        for (String url : urls) {
            System.out.println(String.format("Url : %s", url));
        }

        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder("src/main/resources/crawled-pages");
        config.setMaxPagesToFetch(10);
        config.setPolitenessDelay(1000);
        config.setMaxDepthOfCrawling(3);

        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        robotstxtConfig.setEnabled(false);
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController crawlController = null;

        try {
            crawlController = new CrawlController(config, pageFetcher, robotstxtServer);
            System.err.println(String.format("Reference url : %s", urls.peek()));
            crawlController.addSeed(urls.poll());
            crawlController.startNonBlocking(Crawler.class, 10);

            for (String url : urls) {
                System.err.println(String.format("Adding seed : %s", url));
                crawlController.addSeed(url);
            }

            crawlController.waitUntilFinish();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
