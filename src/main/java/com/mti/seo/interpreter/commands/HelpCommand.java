package com.mti.seo.interpreter.commands;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.Collection;

public class HelpCommand implements Command {
    private Options options;

    public HelpCommand(Options options) {
        this.options = options;
    }

    public void execute() {
        Collection<Option> options = this.options.getOptions();

        for (Option option : options) {
            System.out.println(String.format("%s or %s : %s", option.getOpt(), option.getLongOpt(), option.getDescription()));
        }

    }
}
