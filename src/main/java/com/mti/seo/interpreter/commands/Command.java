package com.mti.seo.interpreter.commands;

public interface Command {
    public void execute();
}
