package com.mti.seo.interpreter.commands;

import com.mti.seo.config.CrawlerConfig;
import com.mti.seo.crawler.Crawler;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class CrawlWithoutFileCommand implements Command {
    private String url;

    public CrawlWithoutFileCommand(String url) {
        this.url = url;
    }

    public void execute() {
        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder("src/main/resources/crawled-pages");
        config.setMaxPagesToFetch(-1);
        config.setPolitenessDelay(1000);
        config.setMaxDepthOfCrawling(3);

        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        robotstxtConfig.setEnabled(false);
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController crawlController = null;

        if (this.url != null && !this.url.isEmpty()) {

            CrawlerConfig.setReferenceUrl(this.url);

            try {
                crawlController = new CrawlController(config, pageFetcher, robotstxtServer);
                crawlController.addSeed(url);
                crawlController.start(Crawler.class, 10);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
