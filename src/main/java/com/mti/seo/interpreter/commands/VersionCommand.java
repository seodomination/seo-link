package com.mti.seo.interpreter.commands;

public class VersionCommand implements Command{

    public void execute() {
        System.out.println(String.format("Program version : %s", 1.0));
    }
}
