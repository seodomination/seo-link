package com.mti.seo.toolbox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by coppen_g
 */
public class ListUtils {

    public static <T> List<T> createList(T... elements) {
        ArrayList<T> list = new ArrayList<>();

        Arrays.stream(elements).forEach(list::add);
        return list;
    }

}
