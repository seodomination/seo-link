package com.mti.seo.vectorizer;

import com.mti.seo.models.Page;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by coppen_g
 */
public class ComparatorTest {
    private BaseComparator baseComparator = new Comparator();

    @Test
    public void compareBasic() {
        Map<String, Float> dictionary1 = new HashMap<>();
        Map<String, Float> dictionary2 = new HashMap<>();

        dictionary1.put("test", 1F);
        dictionary1.put("testify", 2F);
        dictionary1.put("foo", 1F);

        dictionary2.put("test", 2F);
        dictionary2.put("testify", 1F);
        dictionary2.put("foo", 1F);

        Page page1 = new Page("test.fr", null, null, dictionary1, null, null);
        Page page2 = new Page("foo.bar", null, null, dictionary2, null, null);

        Page result = this.baseComparator.compare(page1, page2);
        Assert.assertEquals("test.fr", result.getPageUri());
    }

    @Test
    public void compareNull() {
        Map<String, Float> dictionary1 = new HashMap<>();
        Map<String, Float> dictionary2 = new HashMap<>();

        dictionary1.put("test", 0F);
        dictionary1.put("testify", 0F);
        dictionary1.put("foo", 1F);

        dictionary2.put("test", 2F);
        dictionary2.put("testify", 1F);
        dictionary2.put("foo", 1F);

        Page page1 = new Page("test.fr", null, null, dictionary1, null, null);
        Page page2 = new Page("foo.bar", null, null, dictionary2, null, null);

        Page result = this.baseComparator.compare(page1, page2);
        Assert.assertNull(result);
    }
}
