package com.mti.seo.vectorizer;

import com.mti.seo.config.CrawlerConfig;
import com.mti.seo.models.*;
import com.mti.seo.toolbox.ListUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class LemmatizerTest {
  private Lemmatizer lem = new Lemmatizer();

  /*
  @Test
  public void findMatch() throws Exception {
    try {
      InputStream in = getClass().getClassLoader().getResourceAsStream
          ("lemmatizer-tests.json");
      String json = IOUtils.toString(in, Charset.defaultCharset());
      JsonReader reader = new JsonReader(json);
      reader.readStartDocument();
      reader.readStartArray();
      String base;
      String expected;
      String actual;

      while (reader.readBsonType() == BsonType.DOCUMENT) {
        reader.readStartDocument();
        base = reader.readString("base");
        expected = reader.readString("expected");
        actual = lem.findMatch(base);
        assertEquals(expected, actual);
        reader.readEndDocument();
      }
      reader.close();
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }
  */

  @Test
  public void applyWeights() {
    List<String> words = ListUtils.createList("foo", "bar", "test", "foo");
    List<Float> result = this.lem.applyWeights(words);

    final Float fooWeight = 2F / words.size();
    final Float otherWeight = 1F / words.size();

    Assert.assertEquals(otherWeight, result.get(1));
    Assert.assertEquals(otherWeight, result.get(2));
    Assert.assertEquals(fooWeight, result.get(0));
  }

  @Test
  public void computeIDFWeights() {
    List<String> wordsList1 = ListUtils.createList("test", "foo", "foo", "bar", "testify");
    List<String> wordsList2 = ListUtils.createList("test", "foo", "bar", "bar", "bar");
    List<Float> tfWeights1 = ListUtils.createList(0.2F, 0.4F, 0.2F, 0.2F);
    List<Float> tfWeights2 = ListUtils.createList(0.2F, 0.2F, 0.6F);

    Page page1 = new Page("test.com", wordsList1, null, null, tfWeights1, null);
    Page page2 = new Page("test.fr", wordsList2, null, null, tfWeights2, null);

    List<Page> pageList = ListUtils.createList(page1, page2);

    CrawlerConfig.setReferencePage(page2);
    this.lem.computeIDFWeights(page1, new ArrayList<>(), false);
    List<Float> idfWeights = page1.getIdfWeights();

    final Float testExpectedFrequency = 1F;
    final Float fooExpectedFrequency = 1F;
    final Float testifyExpectedFrequency = 0.5F;

    Assert.assertFalse(idfWeights.isEmpty());
    Assert.assertEquals(4, idfWeights.size());
    Assert.assertEquals(testExpectedFrequency, idfWeights.get(0));
    Assert.assertEquals(fooExpectedFrequency, idfWeights.get(1));
    Assert.assertEquals(testifyExpectedFrequency, idfWeights.get(3));
  }

  @Test
  public void createVector() {
    List<String> words = ListUtils.createList("test", "bar", "bar", "testify");
    List<Float> tfWeights = ListUtils.createList(0.25F, 0.5F, 0.25F);
    List<Float> idfWeights = ListUtils.createList(1F, 0.75F, 0.25F);

    final Float expectedTestWeight = 0.25F;
    final Float expectedBarWeight = 0.375F;
    final Float expectedTestifyWeight = 0.0625F;

    Page page = new Page("test.com", words, null, null, tfWeights, idfWeights);
    this.lem.createPageVector(page);

    Assert.assertFalse(page.getVectors() == null);
    Assert.assertFalse(page.getVectors().isEmpty());

    final Float testWeightResult = page.getVectors().get("test");
    final Float barWeightResult = page.getVectors().get("bar");
    final Float testifyWeightResult = page.getVectors().get("testify");

    Assert.assertEquals(expectedTestWeight, testWeightResult);
    Assert.assertEquals(expectedBarWeight, barWeightResult);
    Assert.assertEquals(expectedTestifyWeight, testifyWeightResult);
  }

  @Test
  public void findMatchTest() {
    Assert.assertTrue(this.lem.findMatch("à") != null);
  }
}
